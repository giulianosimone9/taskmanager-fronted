import { BACKEND_URL } from '$env/static/private';
import type { ProcessInterface, MemoryInterface} from '$lib';
export async function serverRequest({token,url,data, type="POST"}:{token:string,url:string,data?:any, type?:string}){
    let response =  await fetch(url, {
        method: type,
        headers: { "Authorization": `Bearer ${token}`, "Content-Type": "application/json" },
        body: JSON.stringify(data)
    });
    if(response.status!==200){
        return {status:false,httpcode:response.status,data:[]};
    }
    let serverData:any = await response.json();
    return {status:true, data:serverData};
}

export async function updateTable(taskToken:string){
    let serverResponse = await serverRequest({token:taskToken,url:BACKEND_URL+"/manager/process",type:"GET"});
    if(!serverResponse.status){
      console.log("Something Wrong");
      return {items:[],memory:{free:0,total:0}} as {items:ProcessInterface[], memory:MemoryInterface};
    }
    let procesos:ProcessInterface[] = serverResponse.data.process;
    let ram:MemoryInterface = serverResponse.data.memory;
    return {
        items:procesos.map(process=>{
        process.ppid = process.ppid!=1?process.ppid : 0;
        return process;
    }),memory:ram}
  }