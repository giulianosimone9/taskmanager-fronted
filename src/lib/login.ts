import {get} from "svelte/store";
import { passwordStore } from "./store"
import type { Cookies } from "@sveltejs/kit";
export function getToken(cookies:Cookies){
    if(cookies.get("token")){
        return cookies.get("token");
    }
    return get(passwordStore)
}
export function destroyToken(cookies:Cookies){
    if(cookies.get("token")){
        cookies.delete("token");
    }else{
        passwordStore.set("");
    }
}