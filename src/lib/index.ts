// place files you want to import through the `$lib` alias in this folder.
import type { Writable } from "svelte/store";
export interface ProcessInterface{
    [key: string]: number | string | ProcessInterface[];
    pid: number;
    name: string;
    cmd: string;
    ppid: number;
    uid: number;
    cpu: number;
    memory: number;
    childs: ProcessInterface[]
}

export interface MemoryInterface{
    free: number;
    total:number
}

export function arrangeChilds(processes: ProcessInterface[]): ProcessInterface[] {
    let map = new Map<number, ProcessInterface>();
    let rootProcesses: ProcessInterface[] = [];

    // Primero, creamos un mapa de todos los procesos por su pid
    for (let process of processes) {
        map.set(process.pid, process);
    }

    // Luego, iteramos nuevamente sobre los procesos para asignar los hijos a sus padres
    for (let process of processes) {
        if (process.ppid !== 0) {
            let parentProcess = map.get(process.ppid);
            if (parentProcess) {
                if(parentProcess.childs){
                    parentProcess.childs.push(process);
                }else{
                    parentProcess.childs = [process];
                }
            }else{
                process.ppid = 0;
                rootProcesses.push(process);
            }
        } else {
            rootProcesses.push(process);
        }
    }

    return rootProcesses;
}

export function filterProcesses(rootProcesses: ProcessInterface[], filter: string | number): ProcessInterface[] {
    if(!filter){
        return rootProcesses;
    }
    function search(process: ProcessInterface): ProcessInterface | null {
        if (process.pid === filter || process.name.includes(filter.toString())) {
            return process;
        }
        if(process.childs){
            let filteredChilds = process.childs.map(child => search(child)).filter(child => child !== null) as ProcessInterface[];
            if (filteredChilds.length > 0) {
                return {...process, childs: filteredChilds};
            }
        }
        return null;
    }

    return rootProcesses.map(process => search(process)).filter(process => process !== null) as ProcessInterface[];
}

export async function killProcessRequest(pid:number|null){
    if(pid==null){
        return;
    }
    let response = await fetch("/task/kill",{
        method:"POST",
        headers:{
            "Content-type":"Application/json"
        },
        body:JSON.stringify({pid})
    })
    let responseData = await response.json();
    if(!responseData.status){
        throw new Error("There was an error with the request");
    }
    return responseData;
}
