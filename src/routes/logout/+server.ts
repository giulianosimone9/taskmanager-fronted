import { redirect } from '@sveltejs/kit';
import { destroyToken, getToken } from "$lib/login";
import type { RequestHandler } from '@sveltejs/kit';
export const GET:RequestHandler = async ({cookies}) => {
    const taskToken = getToken(cookies);
    if(!taskToken){
        throw redirect(302,"/");
    }
    destroyToken(cookies);
    throw redirect(302,"/");
}