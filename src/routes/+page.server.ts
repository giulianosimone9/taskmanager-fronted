import type { Actions, PageServerLoad } from './$types';
import { BACKEND_URL } from '$env/static/private';
import { passwordStore } from '$lib/store';
import { redirect } from '@sveltejs/kit';

import { getToken } from "$lib/login";

 
export const load = (async ({ cookies }) => {

    if(getToken(cookies)){
        throw redirect(302,"/task")
    }
    return {};

}) satisfies PageServerLoad;


export const actions = {
	login: async ({cookies,request}) => {
		const data = await request.formData();
		
        const remember = (Boolean)(data.get("remember"));
        let success = false;
        (remember);
        try{
            let serverResponse = await fetch(BACKEND_URL + "/login",{
                method:"POST",
                headers:{
                    'Content-Type': 'application/json',
                },
                body:JSON.stringify({password:data.get("password")})
            })

            let httpcode = serverResponse.status;
            if(httpcode!=200){
                return {success:false,httpcode};
        }

        const responseData:{ token: string } = await serverResponse.json();
        if(remember){
            cookies.set("token",responseData.token);
        }else{
            passwordStore.set(responseData.token);
        }
        success = true;
        }catch(err){
            success = false;
        }

            if(success){
                throw redirect(302,"/task")
            }else{
                return {success,httpcode:500};
            }

        
        

	}
} satisfies Actions;