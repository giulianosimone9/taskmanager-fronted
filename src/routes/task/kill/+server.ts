import { json } from '@sveltejs/kit';
import {serverRequest} from "$lib/server";
import { redirect } from '@sveltejs/kit';
import { getToken } from "$lib/login";
import type { RequestHandler } from '@sveltejs/kit';
import { BACKEND_URL } from '$env/static/private';
export const POST:RequestHandler = async ({cookies,request}) => {
    const taskToken = getToken(cookies);
    const {pid} = await request.json() as {pid:number};
    if(!taskToken){
        throw redirect(302,"/");
    }
	let response = await serverRequest({token:taskToken,url:BACKEND_URL+"/manager/kill",data:{pid}})
    return json(response);
   
}