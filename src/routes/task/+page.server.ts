import { redirect } from '@sveltejs/kit';
import { getToken } from "$lib/login";
import { BACKEND_URL } from '$env/static/private';
import {updateTable} from "$lib/server";
import type { PageServerLoad } from './$types';
import type { ProcessInterface } from '$lib';


 
export const load = (async ({ cookies }) => {
    const taskToken = getToken(cookies);
    if(!taskToken){
        throw redirect(302,"/")
    }
    let {items,memory} = await updateTable(taskToken);
    return {
        taskToken,
        BACKEND_URL,
        items,
        memory
    };

}) satisfies PageServerLoad;


