import { json } from '@sveltejs/kit';
import {updateTable} from "$lib/server";
import { redirect } from '@sveltejs/kit';
import { getToken } from "$lib/login";
import type { RequestHandler } from '@sveltejs/kit';
export const GET:RequestHandler = async ({cookies}) => {
    const taskToken = getToken(cookies);
    if(!taskToken){
        throw redirect(302,"/");
    }
    let response = await updateTable(taskToken);
	return json(response);
}