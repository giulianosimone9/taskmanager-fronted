# TaskManager FrontEnd

## Simple TaskManager FrontEnd for [this backend](https://gitlab.com/giulianosimone9/taskmanagernode) made with Sveltekit, flowbite for Svelte and Vite
 - Manage your server resources with only your browser
- Keep it safe. Use a password to access
![login screen](./repoImages/login.png)
- See all your process and a summary of the resources
![task manager screen](./repoImages/taskManager.png)
- Kill the process you want with right click
![kill process](./repoImages/killProcess.png) 
- Or use double click
![double click](./repoImages/doubleClick.png)

### Configuration
Only complete the .env file
`BACKEND_URL = "BACKEND_URL:BACKEND_PORT"`
Then run `npm install`
